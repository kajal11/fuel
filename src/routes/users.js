const express = require('express');
const { getToken } = require( '../models/userModel' );
const router = express.Router();

const userController = require('../controllers/userController');

// GET request for User
router.get('/profile', userController.user_profile_get);
router.get('/profile-user', userController.users_profile_get);

// GET request for updating User settings
router.get('/settings', userController.user_settings_get);

router.get('/settings-user', userController.users_settings_get);
router.post('/settings-user',getToken, userController.users_settings_post);

// GET request for change password
router.get('/change_password', userController.change_password_get);


router.get('/change-password-user', userController.user_change_password_get);
// GET request fuel quote
router.get('/request_fuel_quote', userController.user_request_fuel_quote_get);

// GET request customer
router.get('/customer', userController.user_customer_get);

// POST request for updating User settings
router.post('/settings', getToken, userController.user_settings_post);



module.exports = router;