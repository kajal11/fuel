
const express = require('express');
const router = express.Router();
const { getToken } = require( '../models/userModel' );
const viewController = require('../controllers/viewController');

router.get('/', viewController.getIndex);

// POST request for creating User
router.post('/sign-up', viewController.signUp_post);

// POST request for loggin-in User
router.post('/login', viewController.login_post);

// GET request for Quote Request Form
router.get('/request-quote', viewController.quote_request_get);

// POST request for Quote Request Form
router.post('/request-quote', getToken, viewController.quote_request_post);

router.get('/request-quote-user',viewController.quote_request_get_user);

//router.post('/request-quote-user', getToken, viewController.quote_request_post_user);

// GET request for Dashboard
router.get('/dashboard', viewController.dashboard_get);
router.get('/dashboardnew', viewController.dashboardnew_get);

// GET request for Customers Add Form
router.get('/request-customer', viewController.customers_request_get);

// POST request for Customer Add Form
router.post('/request-customer', getToken, viewController.customers_request_post);

// GET request for Customer management
router.get('/customers', viewController.customers_get);

// GET request for Quotes management
router.get('/quotes', viewController.quotes_get);
router.get('/quotes-user', viewController.quotes_user_get);

// GET request for invoices management
router.get('/invoices', viewController.invoices_get);

// GET request for payments management
router.get('/payments', viewController.payments_get);

router.get('/payments-user', viewController.payments_user_get);

// GET request for users management
router.get('/users', viewController.users_get);

router.get('/edit-quote', viewController.edit_quote);

router.post('/edit-quote', viewController.edit_quote_post);

router.get('/price-quote', viewController.price_get);
module.exports = router;