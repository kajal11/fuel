import { displayMessage } from "../utilities/message.js";
import { validatedInput } from "./validation.js";
import { loader, removeLoader } from "../utilities/loader.js";
import * as CONFIG from "../config.js";
import { _id } from "../utilities/helper.js";

const submitBtn = _id("sendRequestGetPrice");
const body = document.querySelector('body');
export const Price = async function (form) {
  try {
    loader(body);
    const res = await fetch(`/price-quote`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        "Authorization": `Bearer ${ localStorage.getItem( 'token' ) }`
      },
      body: JSON.stringify(form),
    });
    if (!res.ok) throw new Error(`${res.statusText} (${res.status})`);
    const data = await res.json();
    // console.log( 'data:', data );
    if ( data ) {
      // removeLoader();
      console.log("data",data)
      displayMessage(`Submitted quote request...`, "success", 3, 'request-quote');
      location.assign('/request_fuel_quote_user');
    }
  } catch (err) {
    removeLoader();
    displayMessage(`${err}`, "error", 3, 'settings');
  }
};
validatedInput();
if (submitBtn) {
   {
    submitBtn.addEventListener("click", (e) => {
      e.preventDefault();
      const gallons = _id("newFuelQuote__gallonRequested");
     
      const form = {
        gallons: gallons.value,
        
      };
      Price(form);
    });
  }

}
