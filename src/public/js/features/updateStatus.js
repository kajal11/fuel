import { displayMessage } from "../utilities/message.js";
import { validatedInput } from "./validation.js";
import { loader, removeLoader } from "../utilities/loader.js";
import * as CONFIG from "../config.js";
import { _id } from "../utilities/helper.js";

const updateBtn = _id("sendRequestEditQuoteButton");
const body = document.querySelector('body');
export const update = async function (form) {
  try {
    loader(body);
    const res = await fetch(`/edit-quote`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        "Authorization": `Bearer ${ localStorage.getItem( 'token' ) }`
      },
      body: JSON.stringify(form),
    });
    if (!res.ok) throw new Error(`${res.statusText} (${res.status})`);
    const data = await res.json();
    if (data) {
      removeLoader();
      displayMessage(`Updated account...`, "success", 3, 'settings');
       location.assign('/quote');
    }
  } catch (err) {
    removeLoader();
    displayMessage(`${err}`, "error", 3, 'settings');
  }
};
validatedInput();
if (updateBtn) {
  if (updateBtn.disabled === false) {
    updateBtn.addEventListener("click", (e) => {
      const quote_number = _id("newFuelQuote__QuoteNumber");
      const quote_status = _id("newFuelQuote__ChangeStatus");
      
      const form = {
        quote_number: quote_number.value,
        quote_status: quote_status.value,
        
      };
      update(form);
    });
  }

}
