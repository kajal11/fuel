
const connection = require( '../dbconfig' );
const { verifyToken } = require('../models/userModel');

exports.insertCustomers = async ({
    first_name,
    last_name,
    company,
    email,
    phone,
    address1,
    address2,
    city,
    state,
    zip_code,
    country,
    token,
    callback 
}) => {
    const decoded = verifyToken( token );
    const values = { ClientInformation_ID: decoded.id, first_name, last_name, company, email, phone, address1, address2, city, state, zip_code, country };
    connection.query( 'INSERT INTO Customers SET ?', values, ( error, results, fields ) => {
        if ( error ) {
            return connection.rollback( () => {
                throw error;
            });
        }
        console.log( 'Successfully inserted new quote!' );
        callback({
            user: decoded
        });
    });
};

exports.getCustomer = async ({
    user,
    callback 
}) => {
    const sql = `
        SELECT *
        FROM Customers
        WHERE
            ClientInformation_ID = ?
    `;
    connection.query( sql, user.ID, ( error, results, fields ) => {
        if ( error ) {
            return connection.rollback( () => {
                throw error;
            });
        }
        // console.log( 'results:', results );
        callback({
            customers: results
        });
    });
};
