-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 28, 2021 at 05:41 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fueldb`
--

-- --------------------------------------------------------

--
-- Table structure for table `clientinformation`
--

CREATE TABLE `clientinformation` (
  `ID` int(11) NOT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone_number` varchar(30) DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` char(2) DEFAULT NULL,
  `zip_code` varchar(10) DEFAULT NULL,
  `photo_url` varchar(512) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `profile_percentage` double DEFAULT NULL,
  `Company` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Country` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `clientinformation`
--

INSERT INTO `clientinformation` (`ID`, `first_name`, `last_name`, `email`, `phone_number`, `address1`, `address2`, `city`, `state`, `zip_code`, `photo_url`, `created_at`, `profile_percentage`, `Company`, `Country`) VALUES
(1, NULL, NULL, 'rashmi@gmail.com', '12344456', NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-26 09:45:59', NULL, '', ''),
(2, 'Kajal', 'kaur', 'kju@gmail.com', NULL, 'Hno.348,F.G.C Road Amritsar', 'jjjjjjj', 'Amritsar', 'FL', '14300', NULL, '2021-07-28 19:51:36', NULL, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `ID` bigint(20) NOT NULL,
  `ClientInformation_ID` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `company` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` int(10) NOT NULL,
  `address1` varchar(200) NOT NULL,
  `address2` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `zip_code` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`ID`, `ClientInformation_ID`, `first_name`, `last_name`, `company`, `email`, `phone`, `address1`, `address2`, `city`, `state`, `zip_code`, `country`) VALUES
(1, 1, 'RASHMI', 'PAL', 'AUGMENT', 'rashmi@gmail.com', 2147483647, 'ASHOK NAGAR', 'PHASE-1', 'DELHI', 'DE', '10009', 'India'),
(2, 1, 'kajal', 'gill', 'AUGMENT', 'rashmi@gmail.com', 2147483647, 'FGC Road', 'Amritsar', 'Amritsar', 'MD', '10009', 'India');

-- --------------------------------------------------------

--
-- Table structure for table `fuel_quote`
--

CREATE TABLE `fuel_quote` (
  `ID` bigint(20) NOT NULL,
  `ClientInformation_ID` int(11) DEFAULT NULL,
  `gallons` float DEFAULT NULL,
  `delivery_date` datetime DEFAULT NULL,
  `suggested_price` float DEFAULT NULL,
  `amount_due` float DEFAULT NULL,
  `valid_until` datetime DEFAULT NULL,
  `quote_status` varchar(128) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fuel_quote`
--

INSERT INTO `fuel_quote` (`ID`, `ClientInformation_ID`, `gallons`, `delivery_date`, `suggested_price`, `amount_due`, `valid_until`, `quote_status`, `created_at`) VALUES
(1, 1, 1, '2021-07-27 00:00:00', 400, 650.5, NULL, NULL, '2021-07-26 09:52:41');

-- --------------------------------------------------------

--
-- Table structure for table `usercredentials`
--

CREATE TABLE `usercredentials` (
  `ID` int(11) NOT NULL,
  `password` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `usercredentials`
--

INSERT INTO `usercredentials` (`ID`, `password`, `type`) VALUES
(1, '$2b$10$bGNaPTQ6uuplRIcTX1HyI.MSRCyyHgGEat718jw5sEeU.hcozqdg2', ''),
(2, '$2b$10$M7W01pwu9CBTJroWfCImpOsud1IFBG92dMnLzmkOrYTDafKHvu/Km', '1');

-- --------------------------------------------------------

--
-- Table structure for table `userlogin`
--

CREATE TABLE `userlogin` (
  `ID` int(11) NOT NULL,
  `token` text DEFAULT NULL,
  `type` text DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `userlogin`
--

INSERT INTO `userlogin` (`ID`, `token`, `type`, `updated_at`) VALUES
(1, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJyYXNobWlAZ21haWwuY29tIiwiaWF0IjoxNjI3MjcyOTg1fQ.UFUk4Ifiw6iTmRZ5-LDU78K5CbcNUvFG9OlKo2adVuk', NULL, NULL),
(2, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiZW1haWwiOiJranVAZ21haWwuY29tIiwiaWF0IjoxNjI3NDgyMTMwfQ.G3ecpQeF4r5AChDEdgXJEKoZaEl9YhI8cU7XqDKXhAI', '1', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientinformation`
--
ALTER TABLE `clientinformation`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `fuel_quote`
--
ALTER TABLE `fuel_quote`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ClientInformation_ID` (`ClientInformation_ID`);

--
-- Indexes for table `usercredentials`
--
ALTER TABLE `usercredentials`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `userlogin`
--
ALTER TABLE `userlogin`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientinformation`
--
ALTER TABLE `clientinformation`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fuel_quote`
--
ALTER TABLE `fuel_quote`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `usercredentials`
--
ALTER TABLE `usercredentials`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `clientinformation`
--
ALTER TABLE `clientinformation`
  ADD CONSTRAINT `clientinformation_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `usercredentials` (`ID`) ON DELETE CASCADE;

--
-- Constraints for table `fuel_quote`
--
ALTER TABLE `fuel_quote`
  ADD CONSTRAINT `fuel_quote_ibfk_1` FOREIGN KEY (`ClientInformation_ID`) REFERENCES `clientinformation` (`ID`);

--
-- Constraints for table `userlogin`
--
ALTER TABLE `userlogin`
  ADD CONSTRAINT `userlogin_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `usercredentials` (`ID`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
